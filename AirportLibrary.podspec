Pod::Spec.new do |s|
  s.name     = 'AirportLibrary'
  s.version  = '0.0.1'
  s.summary  = 'AirportLibrary - Airport Library'
  s.homepage = 'thisShouldbeahomepage.com'
  s.author   = { 'Stefan Sabau' => 'stefan.sabau@gmail.com' }
  s.source   = { :git => '',
                 :tag => "#{s.version}" }
  s.license  = { :type => 'personal', :text => 'Copyright Stefan Sabau' }
  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '3.0' }

  s.description = 'Airport Library'

  s.requires_arc = true
  s.platform     = :ios, "10.0"

  s.default_subspec = 'AirportLibrary'

  s.subspec 'AirportLibrary' do |airlib|
    airlib.source_files = 'Sources/**/*.{swift}'
  end
end
