//
//  ViewController.swift
//  ExampleApp
//
//  Created by Stefan Sabau on 25/01/2017.
//  Copyright © 2017 Stefan. All rights reserved.
//

import UIKit
import AirportLibrary

class ViewController: UIViewController {
    
    @IBOutlet weak var statusLabel: UILabel!
    
    let heathrowAirTrafficControll = HeathrowAirTrafficController(airport: Heathrow(), dispatcher: HeathrowLandingDispatcher())
    
    let aircrafts: [Aircraft] = [AnyAircraft(name: "Small plane", size: Size(length: 10, width: 5)), Helicopter(name: "Cobra", size: Size(length: 8, width: 8)), AnyAircraft(name: "Boeing", size: Size(length: 120, width: 20))]
    
    @IBAction func didTapActionButton(_ sender: UIButton) {
        statusLabel.text = "Check console output!"
        let randomAircraftIndex = Int(arc4random_uniform(3))
        let newRandomAircraft = aircrafts[randomAircraftIndex]
        let randomAction = Int(arc4random_uniform(2))
        if randomAction == 0 {
            do {
                try heathrowAirTrafficControll.requestLanding(for: newRandomAircraft)
            } catch {
            }
        } else {
            do {
                try heathrowAirTrafficControll.requestTakeOff(for: newRandomAircraft)
            } catch {
            }
        }
    }
    
}

