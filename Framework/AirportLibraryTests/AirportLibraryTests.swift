//
//  AirportLibraryTests.swift
//  AirportLibraryTests
//
//  Created by Stefan Sabau on 25/01/2017.
//  Copyright © 2017 Stefan. All rights reserved.
//

import XCTest
@testable import AirportLibrary

class Validator: LandingValidator {
}

class AnyLandingDispatcher: LandingDispatcher {
    var validator: LandingValidator = Validator()
}

class AnyAircraft: Aircraft {
    var name: String
    var size: Size
    
    init(name: String, size: Size) {
        self.name = name
        self.size = size
    }
}

class Heathrow: Airport {
    var runways: [RunwayType] = []
    
    required init?(runways: [RunwayType]) {
        self.runways = runways
    }
    
    init() {
        self.runways = [Helipad(length: 10), Runway(length: 10, width: 10), Runway(length: 50, width: 50)]
    }
}

class AirportLibraryTests: XCTestCase {

}
