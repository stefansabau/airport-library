//
//  LandingDispatcherTests.swift
//  AirportLibrary
//
//  Created by Stefan Sabau on 26/01/2017.
//  Copyright © 2017 Stefan. All rights reserved.
//

import XCTest
@testable import AirportLibrary


class LandingDispatcherTests: XCTestCase {
    
    let dispatcher = AnyLandingDispatcher()
    let airport = Heathrow()
    let smallPlane = AnyAircraft(name: "Small Plane", size: Size(length: 10, width: 5))
    let boeing = AnyAircraft(name: "Boeing", size: Size(length: 100, width: 20))
    
    func testMarkRunwayAsFree() {
        let runway = airport.runways[0]
        dispatcher.mark(runway: runway, asFree: true)
        XCTAssert(runway.isFree == true, "Runway should be marked as free.")
    }
    
    func testMarkRunwayAsOccupied() {
        let runway = airport.runways[1]
        dispatcher.mark(runway: runway, asFree: false)
        XCTAssert(runway.isFree == false, "Runway should be marked as occupied.")
    }
    
    func testLandingOfSmallAircraft() {
        do {
            let result = try dispatcher.land(aircraft: smallPlane, onAirport: airport)
            XCTAssert(result.1.isFree == false, "Aircraft should land and runway should be marked as occupied.")
        } catch {
            XCTFail("Landing of small aircraft should succeed.")
        }
    }
    
    func testLandingOfLargeAircraft() {
        do {
            let _ = try dispatcher.land(aircraft: boeing, onAirport: airport)
            XCTFail("Aircraft should NOT land.")
        } catch {
        }
    }
    
    func testAircraftTakeOff() {
        do {
            let result = try dispatcher.land(aircraft: smallPlane, onAirport: airport)
            XCTAssert(result.1.isFree == false, "Aircraft should first land on a runway in order to take off.")
            dispatcher.takeOff(aircraft: smallPlane, fromRunway: result.1)
            XCTAssert(result.1.isFree == true, "Runway should be marked as free after aircraft take off.")
        } catch {
            XCTFail("Landing of small aircraft should succeed.")
        }
    }
}
