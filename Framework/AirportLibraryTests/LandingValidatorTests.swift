//
//  LandingValidatorTests.swift
//  AirportLibrary
//
//  Created by Stefan Sabau on 26/01/2017.
//  Copyright © 2017 Stefan. All rights reserved.
//

import XCTest
@testable import AirportLibrary

class LandingValidatorTests: XCTestCase {
   
    let validator = Validator()
    let boeing = AnyAircraft(name: "Boeing", size: Size(length: 100, width: 20))
    let helicopter = Helicopter(name: "IAR", size: Size(length: 10, width: 10))
    
    func testAircraftCannotLandOnHelipad() {
        let helipad = Helipad(length: 10)
        let canLand = validator.canLand(aircraft: boeing, onRunway: helipad)
        XCTAssert(canLand == false, "Aicraft should not be allowed to land on helipad")
    }
    
    func testAircraftCanLandOnLargeAirstrip() {
        let airstrip = Runway(length: 120, width: 30)
        let canLand = validator.canLand(aircraft: boeing, onRunway: airstrip)
        XCTAssert(canLand == true, "Airport should be allowed to land on a large enough airstrip")
    }
    
    func testAircraftCannotLandOnSmallAirstrip() {
        let smallAirstrip = Runway(length: 80, width: 30)
        let canLand = validator.canLand(aircraft: boeing, onRunway: smallAirstrip)
        XCTAssert(canLand == false, "Airport should NOT be allowed to land on a small airstrip")
    }
    
    func testHelicopterCanLandOnLargeAirstrip() {
        let airstrip = Runway(length: 80, width: 30)
        let canLand = validator.canLand(aircraft: helicopter, onRunway: airstrip)
        XCTAssert(canLand == true, "Airport should be allowed to land on a large enough airstrip")
    }
    
    func testHelicopterCannotLandOnSmallAirstrip() {
        let smallAirstrip = Runway(length: 80, width: 5)
        let canLand = validator.canLand(aircraft: helicopter, onRunway: smallAirstrip)
        XCTAssert(canLand == false, "Airport should be allowed to land on a large enough airstrip")
    }
    
    func testHelicopterCanLandOnLargeHelipad() {
        let helipad = Helipad(length: 11)
        let canLand = validator.canLand(aircraft: helicopter, onRunway: helipad)
        XCTAssert(canLand == true, "Airport should be allowed to land on a large enough airstrip")
    }
    
    func testHelicopterCannotLandOnSmallHelipad() {
        let smallHelipad = Helipad(length: 8)
        let canLand = validator.canLand(aircraft: helicopter, onRunway: smallHelipad)
        XCTAssert(canLand == false, "Airport should be allowed to land on a large enough airstrip")
    }
}
