//
//  AirTrafficControl.swift
//  AirportLibrary
//
//  Created by Stefan Sabau on 26/01/2017.
//  Copyright © 2017 Stefan. All rights reserved.
//

import Foundation

public class AirTrafficControlError: Error {
    public let description: String
    
    public init(description: String) {
        self.description = description
    }
}

public protocol AirTrafficControlType {
    var airport: Airport { get }
    var dispatcher: LandingDispatcher { get }
    func requestLanding(for aircraft: Aircraft) throws
    func requestTakeOff(for aircraft: Aircraft) throws
}

open class AirTrafficControl: AirTrafficControlType {
    public var airport: Airport
    public var dispatcher: LandingDispatcher
    private var occupiedRunways: [String: RunwayType] = [:]
    
    public init(airport: Airport, dispatcher: LandingDispatcher) {
        self.airport = airport
        self.dispatcher = dispatcher
    }
    
    public func requestLanding(for aircraft: Aircraft) throws {
        do {
            let (aircraft, targetRunway) = try dispatcher.land(aircraft: aircraft, onAirport: self.airport)
            occupiedRunways[aircraft.name] = targetRunway
        } catch let error {
            throw error
        }
    }
    
    public func requestTakeOff(for aircraft: Aircraft) throws {
        guard let occupiedRunway = occupiedRunways[aircraft.name] else {
            let errorMessage = "Take off denied. Aircraft: \(aircraft) did not land prior to this"
            print(errorMessage)
            throw AirTrafficControlError(description: errorMessage)
        }
        dispatcher.takeOff(aircraft: aircraft, fromRunway: occupiedRunway)
        occupiedRunways[aircraft.name] = nil
    }
}
