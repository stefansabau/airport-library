//
//  Aircraft.swift
//  AirportLibrary
//
//  Created by Stefan Sabau on 26/01/2017.
//  Copyright © 2017 Stefan. All rights reserved.
//

import Foundation

public protocol Aircraft {
    var name: String { get }
    var size: Size { get }
    
    func land()
    func takeOff()
    func fly()
}

extension Aircraft {
    public func fly() {
        print("\(name) \(type(of: self)) flying")
    }
    
    public func land() {
        print("\(name) \(type(of: self)) landing")
    }
    
    public func takeOff() {
        print("\(name) \(type(of: self)) taking off")
    }
}
