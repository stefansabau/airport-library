//
//  Airport.swift
//  AirportLibrary
//
//  Created by Stefan Sabau on 26/01/2017.
//  Copyright © 2017 Stefan. All rights reserved.
//

import Foundation

public protocol Airport {
    var runways: [RunwayType] { get }
    
    init?(runways: [RunwayType])
}
