//
//  Helicopter.swift
//  AirportLibrary
//
//  Created by Stefan Sabau on 26/01/2017.
//  Copyright © 2017 Stefan. All rights reserved.
//

import Foundation

public class Helicopter: Aircraft {
    public static let minimumRequiredLandingWidth: Double = 10
    
    public var name: String
    public var size: Size
    
    public init(name: String, size: Size) {
        self.name = name
        self.size = size
    }
}
