//
//  Helipad.swift
//  AirportLibrary
//
//  Created by Stefan Sabau on 26/01/2017.
//  Copyright © 2017 Stefan. All rights reserved.
//

import Foundation

public class Helipad: Runway {
    public init(length: Double) {
        super.init(length: length, width: length)
    }
}
