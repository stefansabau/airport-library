//
//  LandingDispatcher.swift
//  AirportLibrary
//
//  Created by Stefan Sabau on 26/01/2017.
//  Copyright © 2017 Stefan. All rights reserved.
//

import Foundation

public protocol LandingDispatcher {
    var validator: LandingValidator { get }
    func mark(runway: RunwayType, asFree isFree: Bool)
    func land(aircraft: Aircraft, onAirport airport: Airport) throws -> (Aircraft, RunwayType)
    func takeOff(aircraft: Aircraft, fromRunway runway: RunwayType)
}

extension LandingDispatcher {
    public func land(aircraft: Aircraft, onAirport airport: Airport) throws -> (Aircraft, RunwayType) {
        guard let freeRunway = airport.runways.first(where: {
            return validator.canLand(aircraft: aircraft, onRunway: $0)
        }) else {
            let errorMessage = "Landing of \(aircraft) refused on airport: \(airport). No free runways"
            print(errorMessage)
            throw AirTrafficControlError(description: errorMessage)
        }
        print("Assigning aircraft \(aircraft) to land on \(freeRunway)")
        aircraft.land()
        self.mark(runway: freeRunway, asFree: false)
        return (aircraft, freeRunway)
    }
    
    public func takeOff(aircraft: Aircraft, fromRunway runway: RunwayType) {
        aircraft.takeOff()
        self.mark(runway: runway, asFree: true)
    }
    
    public func mark(runway: RunwayType, asFree isFree: Bool) {
        runway.isFree = isFree
    }
}
