//
//  LandingValidator.swift
//  AirportLibrary
//
//  Created by Stefan Sabau on 26/01/2017.
//  Copyright © 2017 Stefan. All rights reserved.
//

import Foundation

public protocol LandingValidator {
    func canLand(aircraft: Aircraft, onRunway runway: RunwayType) -> Bool
}

extension LandingValidator {
    public func canLand(aircraft: Aircraft, onRunway runway: RunwayType) -> Bool {
        if aircraft is Helicopter {
            return runway.isFree && runway.size.width >= Helicopter.minimumRequiredLandingWidth
        }
        if runway is Helipad {
            return false
        }
        return runway.isFree && aircraft.size <= runway.size
    }
}
