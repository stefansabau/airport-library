//
//  Runway.swift
//  AirportLibrary
//
//  Created by Stefan Sabau on 26/01/2017.
//  Copyright © 2017 Stefan. All rights reserved.
//

import Foundation

public class Runway: RunwayType {
    public var size: Size
    public var isFree: Bool = true
    
    public init(length: Double, width: Double) {
        self.size = Size(length: length, width: width)
    }
}
