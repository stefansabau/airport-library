//
//  RunwayFactory.swift
//  AirportLibrary
//
//  Created by Stefan Sabau on 26/01/2017.
//  Copyright © 2017 Stefan. All rights reserved.
//

import Foundation

public protocol RunwayFactory {
    func makeRunway(with length: Double, width: Double) -> RunwayType?
    func makeRunway(with size: Size) -> RunwayType?
}

extension RunwayFactory {
    public func makeRunway(with length: Double, width: Double) -> RunwayType? {
        return self.makeRunway(with: Size(length: length, width: width))
    }
}
