//
//  RunwayType.swift
//  AirportLibrary
//
//  Created by Stefan Sabau on 26/01/2017.
//  Copyright © 2017 Stefan. All rights reserved.
//

import Foundation

public protocol RunwayType: class {
    var size: Size { get }
    var isFree: Bool { get set }
}
