//
//  Size.swift
//  AirportLibrary
//
//  Created by Stefan Sabau on 26/01/2017.
//  Copyright © 2017 Stefan. All rights reserved.
//

import Foundation

public struct Size {
    public var length: Double
    public var width: Double
    
    public init(length: Double, width: Double) {
        self.length = length
        self.width = width
    }
}

extension Size: Equatable {
    public static func ==(lhs: Size, rhs: Size) -> Bool {
        return lhs.length == rhs.length && lhs.width == rhs.width
    }
}
extension Size: Comparable {
    public static func <(lhs: Size, rhs: Size) -> Bool {
        return lhs.length < rhs.length && rhs.width < rhs.width
    }
    
    public static func <=(lhs: Size, rhs: Size) -> Bool {
        return lhs.length <= rhs.length && rhs.width <= rhs.width
    }
    
    public static func >=(lhs: Size, rhs: Size) -> Bool {
        return lhs.length >= rhs.length && rhs.width >= rhs.width
    }
    
    public static func >(lhs: Size, rhs: Size) -> Bool {
        return lhs.length > rhs.length && rhs.width > rhs.width
    }
}
